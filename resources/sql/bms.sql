/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : bms

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-08-25 18:54:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `leavebill`
-- ----------------------------
DROP TABLE IF EXISTS `leavebill`;
CREATE TABLE `leavebill` (
  `id` varchar(32) NOT NULL,
  `days` int(10) DEFAULT NULL COMMENT '请假天数',
  `content` varchar(500) DEFAULT NULL COMMENT '请假原因',
  `leveDate` datetime DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `userId` varchar(32) DEFAULT NULL COMMENT '请假人id',
  `state` int(32) DEFAULT NULL COMMENT '请假单状态，0未申请，1审批中，2已完成',
  `businesskey` varchar(100) DEFAULT NULL COMMENT '流程中的BusinessKey，根据这个找流程图',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of leavebill
-- ----------------------------
INSERT INTO `leavebill` VALUES ('2d21707c2e0e4bcc82cb9bdcf0e67ec1', '5', '休息几天', '2017-08-20 13:00:31', '希望领导批准', '0d8f8d9787564ef2a83d1508cfe4ebbb', '2', 'myLeaveBillprocess.2d21707c2e0e4bcc82cb9bdcf0e67ec1');
INSERT INTO `leavebill` VALUES ('e83ce68a1c3f426ca29dbcbdac4b78d4', '4', '回家看望老年人', '2017-08-21 22:41:25', '望批准', '0d8f8d9787564ef2a83d1508cfe4ebbb', '2', 'myLeaveBillprocess.e83ce68a1c3f426ca29dbcbdac4b78d4');

-- ----------------------------
-- Table structure for `sys_dictitem`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictitem`;
CREATE TABLE `sys_dictitem` (
  `id` varchar(50) NOT NULL COMMENT '32位uuid主键',
  `code` varchar(50) DEFAULT NULL COMMENT '编码',
  `key` varchar(50) DEFAULT NULL COMMENT '存储key',
  `value` varchar(50) DEFAULT NULL COMMENT '页面中显示值',
  `seq` int(20) DEFAULT NULL COMMENT '排序字段',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dictitem
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_dicttype`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dicttype`;
CREATE TABLE `sys_dicttype` (
  `id` varchar(50) NOT NULL COMMENT '32位uuid',
  `name` varchar(50) DEFAULT NULL COMMENT '数据字典类型名',
  `code` varchar(50) DEFAULT NULL COMMENT '数据字典编码',
  `descript` varchar(200) DEFAULT NULL,
  `del` tinyint(1) DEFAULT '1' COMMENT '1已删除，0未删除,是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dicttype
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `userId` varchar(32) DEFAULT NULL COMMENT '用户id',
  `userName` varchar(32) DEFAULT NULL COMMENT '用户名',
  `moudle` varchar(100) DEFAULT NULL COMMENT '模块名字',
  `operation` varchar(255) DEFAULT NULL COMMENT '操作',
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('7cb36304fe2d413683ab4d856e0cd1b3', 'c15bdc7fca9a4200b205d0692c56632c', 'root', 'junitTest', 'add1/0', '2017-06-17 20:20:05');
INSERT INTO `sys_log` VALUES ('b0fc587cce63408288b6c86cf0ddb66e', '3aa367f186984a0ca2a69edc6813c8c4', 'root', 'junitTest', 'add', '2017-06-06 19:39:56');

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` varchar(50) NOT NULL,
  `pid` varchar(50) DEFAULT NULL,
  `menuName` varchar(50) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `seq` int(50) DEFAULT NULL COMMENT '菜单同级排序号',
  `state` tinyint(1) DEFAULT '0' COMMENT '1启用，0未启用',
  `del` tinyint(1) DEFAULT '1' COMMENT '1已删除，0未删除,是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('18487a04737143df81bd6f6f197dcb1a', '769fac85cb954e1790c5e6467e6b7083', '制作流程', 'el-icon-caret-bottom', '/ModelControl/skipModelPage', '3', '1', '0');
INSERT INTO `sys_menu` VALUES ('23dd2e01b4714f428dabdaf425d41dc4', '-1', '业务管理', 'el-icon-menu', '', '3', '1', '0');
INSERT INTO `sys_menu` VALUES ('4a012fa5881a440ba177e15168f4868f', '23dd2e01b4714f428dabdaf425d41dc4', '待我审批', 'el-icon-message', '/LeaveBillFlowControl/skipApprovaList', '2', '1', '0');
INSERT INTO `sys_menu` VALUES ('4f5b0f3db31340ffadd7051a53341c21', '8ca1d5c1c050435093990d44ee114b3d', '组织管理', 'el-icon-message', '/SysOrgControl/skiporganizPage', '5', '1', '0');
INSERT INTO `sys_menu` VALUES ('5f9159d7abd64f6495fd4f0add43cedc', '8ca1d5c1c050435093990d44ee114b3d', '权限赋予', 'el-icon-message', '/SysMenuRole/skipMenuRolePage', '4', '1', '0');
INSERT INTO `sys_menu` VALUES ('68664e01782a40cda83542045c464d1f', '8ca1d5c1c050435093990d44ee114b3d', '角色管理', 'el-icon-message', '/SysRoleControl/skipRolePage', '2', '1', '0');
INSERT INTO `sys_menu` VALUES ('6f3b4424737d445ca23608afb30f1514', '8ca1d5c1c050435093990d44ee114b3d', '用户管理', 'el-icon-message', '/UserControl/skipUserPage', '3', '1', '0');
INSERT INTO `sys_menu` VALUES ('769fac85cb954e1790c5e6467e6b7083', '-1', '流程管理', 'el-icon-loading', '', '2', '1', '0');
INSERT INTO `sys_menu` VALUES ('7b32fe75f1e348da996492e595579a1b', '8ca1d5c1c050435093990d44ee114b3d', '菜单管理', 'el-icon-message', '/SysMenuControl/skipMenuPage', '1', '1', '0');
INSERT INTO `sys_menu` VALUES ('7cf35179f56648be897cfecae3f15175', '23dd2e01b4714f428dabdaf425d41dc4', '请假管理', 'el-icon-message', '/LeaveBillControl/skipLeaveBillListPage', '1', '1', '0');
INSERT INTO `sys_menu` VALUES ('8ca1d5c1c050435093990d44ee114b3d', '-1', '系统管理', 'el-icon-setting', '', '1', '1', '0');
INSERT INTO `sys_menu` VALUES ('b08598f3835a4aa1b883111163706582', '769fac85cb954e1790c5e6467e6b7083', '可用流程', 'el-icon-message', '/ModelControl/skipDeployPage', '2', '1', '0');
INSERT INTO `sys_menu` VALUES ('f92ba77775754e4e83582a4c57554141', '-1', '多数据源', 'el-icon-caret-bottom', 'getActivity/getUser', null, '1', '1');

-- ----------------------------
-- Table structure for `sys_organiz`
-- ----------------------------
DROP TABLE IF EXISTS `sys_organiz`;
CREATE TABLE `sys_organiz` (
  `id` varchar(50) NOT NULL COMMENT '32位uuid',
  `organizName` varchar(50) DEFAULT NULL COMMENT '组织名称',
  `pid` varchar(50) DEFAULT NULL COMMENT '上级id',
  `descript` varchar(200) DEFAULT NULL,
  `del` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1已删除，0未删除,是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_organiz
-- ----------------------------
INSERT INTO `sys_organiz` VALUES ('815dbafc79a941239644a59bb44ed7d2', '教务处', '-1', '', '0');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` varchar(50) NOT NULL DEFAULT '' COMMENT '32位uuid',
  `pid` varchar(50) DEFAULT NULL COMMENT '上级id',
  `roleName` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `descript` varchar(200) DEFAULT NULL,
  `del` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1已删除，0未删除,是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('079501dbac8947d3bc578ece52ae6df8', '-1', '普通访客', '', '1');
INSERT INTO `sys_role` VALUES ('129b0bdb7f8f41a6bb926c251ebb2b52', '7e1c343ba13349179fdfa4041849dc1b', '客户经理', '', '1');
INSERT INTO `sys_role` VALUES ('5ee7a7cdedda4138ba7f9cd0666dd11d', '9ef7aa5425ee45b9abe52f5fffd6401f', '普通访客', '简单浏览网页', '0');
INSERT INTO `sys_role` VALUES ('72fede9f97714bbe932ddad0cfbce741', 'cd57922151144422a761a3a1d2754f89', '运维人员', null, '0');
INSERT INTO `sys_role` VALUES ('7e1c343ba13349179fdfa4041849dc1b', '-1', '客户', '', '0');
INSERT INTO `sys_role` VALUES ('9ef7aa5425ee45b9abe52f5fffd6401f', '-1', '访客', '', '0');
INSERT INTO `sys_role` VALUES ('ac6a2a343ed14247b50d193616f0dbdc', 'cd57922151144422a761a3a1d2754f89', '系统管理员', '', '0');
INSERT INTO `sys_role` VALUES ('cd57922151144422a761a3a1d2754f89', '-1', '管理员', '', '0');
INSERT INTO `sys_role` VALUES ('e3622ade967e4d58a82e905068b4dfae', 'cd57922151144422a761a3a1d2754f89', '超级管理员', '', '0');
INSERT INTO `sys_role` VALUES ('f9a8988704294a59830d5fec57b47b20', '7e1c343ba13349179fdfa4041849dc1b', '客户经理', '', '0');

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `roleId` varchar(50) NOT NULL,
  `menuId` varchar(50) NOT NULL,
  PRIMARY KEY (`roleId`,`menuId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('e3622ade967e4d58a82e905068b4dfae', '-1');
INSERT INTO `sys_role_menu` VALUES ('e3622ade967e4d58a82e905068b4dfae', '18487a04737143df81bd6f6f197dcb1a');
INSERT INTO `sys_role_menu` VALUES ('e3622ade967e4d58a82e905068b4dfae', '23dd2e01b4714f428dabdaf425d41dc4');
INSERT INTO `sys_role_menu` VALUES ('e3622ade967e4d58a82e905068b4dfae', '4a012fa5881a440ba177e15168f4868f');
INSERT INTO `sys_role_menu` VALUES ('e3622ade967e4d58a82e905068b4dfae', '4f5b0f3db31340ffadd7051a53341c21');
INSERT INTO `sys_role_menu` VALUES ('e3622ade967e4d58a82e905068b4dfae', '5f9159d7abd64f6495fd4f0add43cedc');
INSERT INTO `sys_role_menu` VALUES ('e3622ade967e4d58a82e905068b4dfae', '68664e01782a40cda83542045c464d1f');
INSERT INTO `sys_role_menu` VALUES ('e3622ade967e4d58a82e905068b4dfae', '6f3b4424737d445ca23608afb30f1514');
INSERT INTO `sys_role_menu` VALUES ('e3622ade967e4d58a82e905068b4dfae', '7b32fe75f1e348da996492e595579a1b');
INSERT INTO `sys_role_menu` VALUES ('e3622ade967e4d58a82e905068b4dfae', '7cf35179f56648be897cfecae3f15175');
INSERT INTO `sys_role_menu` VALUES ('e3622ade967e4d58a82e905068b4dfae', 'b08598f3835a4aa1b883111163706582');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` varchar(50) NOT NULL COMMENT '用户表主键',
  `userName` varchar(100) DEFAULT NULL COMMENT '用户名',
  `userNameEn` varchar(50) DEFAULT NULL COMMENT '用户英文名',
  `account` varchar(100) NOT NULL COMMENT '帐号',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `sex` varchar(20) DEFAULT NULL COMMENT '性别',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `tel` varchar(50) DEFAULT NULL COMMENT '电话号',
  `roleId` varchar(50) DEFAULT NULL COMMENT '角色id',
  `organizId` varchar(50) DEFAULT NULL COMMENT '组织id',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUesrId` varchar(50) DEFAULT NULL COMMENT '创建用户id',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUesrId` varchar(50) DEFAULT NULL,
  `seq` int(100) DEFAULT NULL COMMENT '排序字段',
  `del` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1已删除，0未删除,是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('009957e0b5de4f0990b62da959ca6ac2', null, null, 'root42', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('017691bb33024893af0c5113ca3a9d76', null, null, 'root95', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('03455383c703453caf8ed077ab77a898', null, null, 'root20', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('04e22fcca7004715bdde00628319c622', null, null, 'root18', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('0cfb333f25b24379b6a830bbc88cad7b', null, null, 'root65', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('0d8f8d9787564ef2a83d1508cfe4ebbb', null, null, 'root', '2G31GBXYA4X3C0FX5E4BG4GE43A035GG', null, null, null, 'e3622ade967e4d58a82e905068b4dfae', null, '2017-06-25 20:10:56', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('0dd6cfa9139241a9b06abc87e0adbefd', null, null, 'root58', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('0edc342976b046c0b8b7d066a009257a', null, null, 'root33', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('0f8d5cb37b3143d4a1b625a7cfd542f8', null, null, 'root44', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('14179909c9e046958e5d6d1de2df44df', null, null, 'root11', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '1');
INSERT INTO `sys_user` VALUES ('14515fe7103a42dabe4f73f37bf53464', null, null, 'root19', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('168d3067a8da4bb6a9c4476bd18cdd9d', null, null, 'root74', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('1c107e5954c04c06856fbc7fca4be62c', null, null, 'root43', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('1c12c6ab172d4aa4944c5e2c0a613b9a', null, null, 'root87', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('1c29646e12d84b6fa7d2d71e4745754a', null, null, 'root92', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('1d29d0569f5548ab99c0881b28cef2f8', null, null, 'root54', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('1eadd590b19c42ee8c0cdc7167336afe', null, null, 'root27', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('21d0e37648624c1f943c9ee319e6d7cc', null, null, 'root25', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('26e3e438807b4f22bcea560467f03df0', null, null, 'root94', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('278b1f57dfa747218e0c39fac57e177b', null, null, 'root62', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('2df59ececfba4ef8b3ca22f1444ffe2d', '总教务领导', null, 'admin', '4X3GXE5X4EYDFX5C0X54BF2ZA2YF20FC', '男', '123456@qq.com', null, 'e3622ade967e4d58a82e905068b4dfae', '815dbafc79a941239644a59bb44ed7d2', '2017-07-30 14:22:01', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('32d3560f4e6940cfa027978b1aa265a6', null, null, 'root21', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('355224fc227f4735aab35ff531aa81bb', null, null, 'root51', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('355e2715364e4732aba5f2ca35d863f3', null, null, 'root64', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('3780f73b19c348588aabe2903a7156b1', null, null, 'root30', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('3b323b553f5142448bf4c63987b50302', null, null, 'root38', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('3b361d787c7f47a895a33c269a531de7', null, null, 'root39', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('4038598847ad40f785ec6a1b77650038', null, null, 'root29', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('410fb71fa22e45428bf24d0cc7a4e8cb', null, null, 'root59', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('41fc23b695df4a38bbe717636a4348c8', null, null, 'root16', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('421eefe23b4e48d0b3cdaf9a75358419', null, null, 'root56', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('422d370b53a04c4aaba343c95269bee6', null, null, 'root81', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('438ec616540a4b4da9b78c6fb2428873', null, null, 'root40', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('444f334c2f174c18b772ca2f94ab0f3b', null, null, 'root8', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('49ed6821dbbc45ee8af250a504082eac', null, null, 'root47', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('4d545a0d40ff49b8977321818e1973d0', null, null, 'root73', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('4d96df557bb84493a8b841e1eb8873a9', null, null, 'root15', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('4ea715cd21594a0f8883da238b8ca1ce', null, null, 'root61', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('513df46db2f34d0aa61c497f3720b725', null, null, 'root90', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('5177bb82842e4b088f494ddd1c2f82f1', null, null, 'root99', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('54d0e7c48e084d15875a4cd338932e8a', null, null, 'root97', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('55295e315f97482d8f8d867628748fcb', null, null, 'root53', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('55afd01cd009420096896ebb8e312607', null, null, 'root71', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('5792ed965176433081b40c413a625109', null, null, 'root41', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('6089f44a3f5744e1a384c802620e98e5', null, null, 'root50', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('60d6f6c565d549458c52342e1fbbddef', null, null, 'root96', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('687b9e5da9974c47afb9e8514151f93a', null, null, 'root31', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('6acfcf7be833498ca2ed844b4baeb17c', null, null, 'root48', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('6f3f04a38df84187b11d2b69420812da', null, null, 'root76', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('6f465f8cb8354e20ada4cd54c2a55ace', null, null, 'root79', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('753043ce7e0441af998fbf1321b4cb9a', null, null, 'root67', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('7550f4e45ccc43deb586fafdcea46cdf', null, null, 'root89', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('76db2c0243dc45f1bd30e0e00bfae3fc', null, null, 'root26', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('775f5c7c538c46a88acab44ee8a7687f', null, null, 'root7', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('7b62f46b706f4a2194b02b100ba51e2d', null, null, 'root83', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('7cf9d362f753471c8f015f693132d8b4', null, null, 'root9', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('7f09078ea0114ef2a5559a2e0ec04a5b', null, null, 'root82', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('80e11d09597a4cacae1d29004f1f54c4', null, null, '123', 'CAC21ZGC02FZAXF1ZGE1AXBFC3CDE1XA', null, null, null, null, null, '2017-07-24 21:53:00', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('811cb5f39d454be781e21911f644bed3', null, null, 'root22', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('89a142124b9c41a2b2557c5ae5d7f342', null, null, 'root10', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('8b47490ce054401ca1a5f0be93f02929', null, null, 'root35', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('8c6edb24c64b44b19c5a281586780d47', null, null, 'root77', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('8da4d440ff2a4e86b2d733087a3979da', null, null, 'root88', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('8e36280848ce47d599c8c8c535f90502', null, null, 'root57', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('904a2fe39d44479c93073c4f36c80b59', null, null, 'root68', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('9546af4c421f44cc8a40d33905d76954', null, null, 'root3', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('95f75a17c9fe49bb8a9716553364f519', null, null, 'root2', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('98136137599741eca764e7eecc6daabc', null, null, 'root28', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('98fc0257d197485ba17e52e392504556', null, null, 'root98', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('9ae7debf14ab4a69b3dc273f70f363fe', null, null, 'root91', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('9aeeaa36e1144f88884348efbb81972f', null, null, 'root46', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('9fe55571ec1849c5bac14e9a15ee885d', null, null, 'root0', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('a94ae9a8ab0d41aca5c16919ba4a058d', null, null, 'root100', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('ae443604f07e490ab7c173602d55e1dd', null, null, 'root1', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('b2790c410a934edcb963351934de11d0', null, null, 'root69', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('b34191db2cc746f1b16279603e319f6b', null, null, 'root72', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('b4cd1faa0b174363a91c8525e90f75d7', null, null, 'root85', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('b980315c1f7f40949d48e16167c6cfb3', null, null, 'root78', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('bbfdb5400bb44d37a84f7cf45c7c07a6', null, null, 'root84', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('bd7aeff41d154cb296f646e12286caba', null, null, 'root55', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('bff4f7065c874b35b603172f45bb5eb1', null, null, 'root66', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('c0a4b774ee88415085bccb822e2ff700', null, null, 'root34', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('c20a932cbeab4043880754273ed6ce68', null, null, 'root5', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('c74fb9678d7045be9b1dda0a12f302b3', null, null, 'root12', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('c93781c7f8d144cf83f14c6d3ba74b3b', null, null, 'root60', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('c973fcd78fd84bada4bcd9178e79be33', null, null, 'root4', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('cb989b3ce40445689cde43ca5e0934d4', null, null, 'root17', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('d13d91a0c94f4473977347d775968f74', null, null, 'root63', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('d1984b1ffc3045f8b36c97605025da73', null, null, 'root49', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('d988cc86453c44f99b3692a9ec764cc5', null, null, 'root23', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('da1cd49ca86845eda7c2a6744ae3bbee', null, null, 'root93', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('dcaecbe1c08c49508beba1a5eb9bec5d', null, null, 'root86', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('dcc128abb24f4a56a725d24fe1b55c45', null, null, 'root13', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('de1cfb7ad1254d0e925d69bf9a270739', null, null, 'root70', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('e3d0228d3909492a928d7116f1df0fbb', null, null, 'root52', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('e4b349673adc4ad09c11b0988441535e', null, null, 'root24', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('f0ed151ae8704ad8817bf01c0f45054b', null, null, 'root45', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('f2a454045a58486897184215d0746c49', null, null, 'root32', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('f355c276e40a439ca25885a1c7ac6f7e', null, null, 'root75', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('f6b291f33d814ff1be375c4d22c2e779', null, null, 'root6', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('fa7ee44a29cd468b8e9a8735338895ec', null, null, 'root14', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('fb49c02e8d5843fca7b49bb49278b428', null, null, 'root80', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:48', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('fcbf12db7c2a44e79ffd59aff2e69c7e', null, null, 'root36', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('ffe3141c14484c10a5421c855cbde707', null, null, 'root37', 'GD0Z5A40X11ZYAFAXZG1GEZ4YFEYBYEF', null, null, null, null, null, '2017-07-24 22:30:47', null, null, null, null, '0');
