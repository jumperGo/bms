require(['vue','ELEMENT','base','vue-resource','Message'],
	function(Vue,ELEMENT,baseUtil,VueResource,messageHelper){
	 //安装 vue的ajax功能
    Vue.use(VueResource);
    //安装element
    Vue.use(ELEMENT);
    //安装message
    Vue.use(messageHelper);
    
    var checkName = function(rule, value, callback){
        if(value == '' || value == undefined){
       	 callback(new Error('模型名不能为空！'));
        }
        else{
       	 callback();
        }
    };
    var checkVersion = function(rule, value, callback){
    	var regEx = /([^0-9]+)|(^$)/g;
        if(regEx.test(value)){
       	 callback(new Error('请输入整数！'));
        }
        else{
       	 callback();
        }
    };
    var modelList = new Vue({
    	el:"#modelList",
    	data:{
    		tableData:[],
			getURL:baseUtil.WebRootUrl+"/ModelControl/getModelList",	
			deleteURL: baseUtil.WebRootUrl+"/ModelControl/deleteModel",
			updateURL: baseUtil.WebRootUrl+"/ModelControl/updateModel",
			deployURL:baseUtil.WebRootUrl+"/ModelControl/deployModel",
			addURL:baseUtil.WebRootUrl+"/ModelControl/createModel",
			bmsModelTitle:"",
			bmsModelVisible:false,
			modelRule:{
				name:[{
    	    		validator:checkName,trigger: 'blur'
    	    	}],
    	    	version:[{
    	    		validator:checkVersion,trigger: 'blur'
    	    	}]
			},
			model:{
				name:"",
				version:""
			},
			formLabelWidth: '120px',
    	},
    	mounted:function(){
    		this.getData();
    	},
    	methods:{
    		//获取数据
    		getData(){
		    	 vm = this;
		    	 vm.$http.get(vm.getURL)
		    	 .then((response) =>{
		    		 if(response.data.result == "200"){
		    			 vm.tableData = response.data.dataList;
		    		 }else{
		    			 console.log(response);
		    		 }
		    	 })
		    	 .catch((response) =>{
		    		 console.log(response);
		    	 })
			},
			handleAdd(){
				//window.open(baseUtil.WebRootUrl+"/ModelControl/createModel");
			    for(key in this.model){
			    	this.model[key] = "";
			    };
			    this.bmsModelVisible = true;
			    this.bmsModelTitle = "新建模型";
			},
			saveModel(){
				vm = this;
		    	 vm.$refs.modelForm.validate(function(valid){
		    		 if(valid){
		    			 vm.$http.post(vm.addURL,vm.model,{emulateJSON:true})
		 	    			.then((response) => {
		 	    				vm.info = response.data.info;
		 	    				if(response.data.result == "200"){
		 	    				  vm.bmsModelVisible = false;
		 	    				  vm.$showMess({message:'添加成功!',messType:'success'});
		 	    				}
		 	    				else{
		    					vm.bmsModelVisible = false;
		    					vm.$showMess({message:'添加失败!',messType:'error'});
		    				    }
		 	    				vm.getData();
		 	    			})
		 	    			.catch(function(response) {
		 	    				vm.info="有异常了";
		 	    			    vm.bmsModelVisible = false;
		 	    				vm.$showMess({message:'添加失败!',messType:'error'});
		 	    			}); 
		    			 
		    			 
		    			 
		    		 }
		    	 });
			},
			handleDeploy(index, row){
				vm = this;
				vm.$http.get(vm.deployURL,{"modelId":row.id},{emulateJSON:true})
		    	 .then((response) =>{
		    		 if(response.data.result == "200"){
		    			 vm.$showMess({message:'部署成功!',messType:'success'});
		    			 this.getData();
		    		 }else{
		    			 console.log(response);
		    			 vm.$showMess({message:'部署失败!',messType:'error'});
		    		 }
		    	 })
		    	 .catch((response) =>{
		    		 console.log(response);
		    		 vm.$showMess({message:'部署失败!',messType:'error'});
		    	 })
			},
	    	handleEdit(index, row){
				window.open(baseUtil.WebRootUrl+"/ModelControl/updateModel?modelId="+row.id);
			},
			handleDelete(index, row){
				vm = this;
				vm.$http.get(vm.deleteURL,{"modelId":row.id},{emulateJSON:true})
		    	 .then((response) =>{
		    		 if(response.data.result == "200"){
		    			 console.log(response);
		    			 vm.$showMess({message:'删除成功!',messType:'success'});
		    			 this.getData();
		    		 }else{
		    			 console.log(response);
		    			 vm.$showMess({message:'删除失败!',messType:'error'});
		    		 }
		    	 })
		    	 .catch((response) =>{
		    		 console.log(response);
		    		 vm.$showMess({message:'删除失败!',messType:'error'});
		    	 })
			},
			dateFormat(row,column){
				var date = row[column.property];
				if(date == null){
					return "";
				}else{
					return new Date(date).format("yyyy-MM-DD HH:mm:ss");
				}
		   }
    	}
    });
  
	
});