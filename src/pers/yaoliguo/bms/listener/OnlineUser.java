package pers.yaoliguo.bms.listener;

import javax.servlet.http.HttpSession;

import pers.yaoliguo.bms.entity.SysUser;

public class OnlineUser {
	
	SysUser user;
	
	String sessionId;
	
	HttpSession session;

	public SysUser getUser() {
		return user;
	}

	public void setUser(SysUser user) {
		this.user = user;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

}
