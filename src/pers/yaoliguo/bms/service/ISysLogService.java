package pers.yaoliguo.bms.service;

import java.util.List;
import java.util.Map;

import pers.yaoliguo.bms.entity.SysLog;

/**
 * @ClassName:       ISysLogService
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年6月5日        下午8:32:29
 */
public interface ISysLogService {
	
	int recordLog(SysLog log);
	
	SysLog getById(String id);
	
	List<Map> getAllLog();
	
	int recordLog(String userId,String userName,String moudle,String operation);
	
}
