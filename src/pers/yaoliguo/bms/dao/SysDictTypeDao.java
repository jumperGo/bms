package pers.yaoliguo.bms.dao;

import org.springframework.stereotype.Repository;

import pers.yaoliguo.bms.entity.SysDictType;
@Repository("sysDictTypeDao")
public interface SysDictTypeDao {
    int deleteByPrimaryKey(String id);

    int insert(SysDictType record);

    int insertSelective(SysDictType record);

    SysDictType selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysDictType record);

    int updateByPrimaryKey(SysDictType record);
}