package pers.yaoliguo.bms.dao;

import org.springframework.stereotype.Repository;

import pers.yaoliguo.bms.entity.SysDictItem;
@Repository("sysDictItemDao")
public interface SysDictItemDao {
    int deleteByPrimaryKey(String id);

    int insert(SysDictItem record);

    int insertSelective(SysDictItem record);

    SysDictItem selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysDictItem record);

    int updateByPrimaryKey(SysDictItem record);
}